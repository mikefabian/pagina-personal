# README

## Preparación de VPS para proyectos con RoR

- Referencias adicionales:
  https://evanwill.github.io/_drafts/notes/ruby-rbenv.html
- Instalar git (Fuente: https://gorails.com/setup/ubuntu/20.04)

```console
sudo apt-get update
sudo apt-get install git-all
```

Configura git

```console
git config --global color.ui true
git config --global user.name "miguel"
git config --global user.email "miguel.fabian.bernal@gmail.com"
ssh-keygen -t rsa -b 4096 -C "miguel.fabian.bernal@gmail.com"
```

Obtener llave y testear conexión con gitlab:

```console
cat ~/.ssh/id_rsa.pub
ssh -T git@gitlab.com
```

- Instalación de algunas dependencias para RoR

```console
sudo apt-get install curl git-core zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn
```

- Instalación de rbenv

Se descarga desde Github

```console
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
```

Se agrega la ubicación del repo para poder ejcutar comando ruby

```console
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
source ~/.bashrc
```

- Instalacion de ruby-build como un pluggin de rbenv (https://github.com/rbenv/ruby-build#readme):

```console
  git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
  echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
  exec $SHELL
```

- Instalacion de Ruby:

```console
rbenv install --list
rbenv install 3.0.1
```

Global se usa para configurar la version de Ruby en toda la compu. Este versión se puede sobreescribir con un archivo .ruby-version en cada aplicación en especifico Ver (https://github.com/rbenv/rbenv#rbenv-global) o por sesion de shell.

```console
rbenv global 3.0.1
```

- Instacion de Rails

```console
gem install rails -v 6.0.2.2
rbenv rehash
```

- Instalación Yarn

```console
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
```

- Instalación de bundler

```console
gem install bundler
gem update --system
```

- Instalacion de node

```console
sudo apt install npm
```

- Instalación capistrano:
  Seguir pasos del repo de github
  Instalacion de Capistrano rails : https://github.com/capistrano/rails
  Seguir los pasos que hay en los repos de todos los pluggins para capistrano.

- Creación de secret_key_base:
  Correr rake secret y copiarlo en development.yml.enc y production.yml.enc
- Aegurarse que se esta adjuntando el production.key en el deploy con:
  set :linked_files, fetch(:linked_files, []).push("config/credentials/production.key")
  Subir el archivo con scp a la ruta indicada antes hacer el deploy.

- Levantar la aplicación:

  Ahora, Nginx esta configurado para que levante paginas desde var/www/..., pero
  nuestra aplicación ahora esta en home/miguel/apps/....
  tenemos que cambiar la configuración para que acepte esta nueva ruta.
  Seguir estos pasos para modificar el sites-avaiable:

  https://matthewhoelter.com/2020/11/10/deploying-ruby-on-rails-for-ubuntu-2004.html

  Al intentar acceder a la página, obtengo un error:

  connect() to unix:///home/miguel/apps/release30_production/shared/tmp/sockets/release30_production-puma.sock failed (2: No such file or directory) while connecting to upstream, client: 179.6.213.141, server: release30.com, request: "GET / HTTP/1.1", upstream: "http://unix:///home/miguel/apps/release30_production/shared/tmp/sockets/release30_productioduction-puma.sock:/", host: "www.release30.com"

  Lo cual indica que esta intentando levantar el servidor, pero no encuentra el socket de la aplicacion puma.
  Verificando con bundle exec production puma:status vemos que el servicio no esta arriba.

  Se tuvo que:

  Agregar install_plugin Capistrano::Puma::Daemon en capfile
  para que al hacer deploy capistrano levante el server, caso contrario solo sube los archivos, lo cual tiene sentido,
  recordar que capistrano y sus librerias son tasks ya armados que se ejecutan autoaticamente a medida que los vamos agregando, no lo teniamos, no
  se ejecuto.

  Luego hay que agregar set :rbenv_map_bins, %w{rake gem bundle ruby rails puma pumactl} para bundle pueda ser reconocido como comando.

  Luego con bundle exec cap production puma:status se comprobó que el servidor esta arriba.

  El problem del socket persistia, sin embargo lo que se hizo fue cambiar la linde de upload del nginx:

  server unix:///home/miguel/apps/release30_production/shared/tmp/sockets/puma.sock;
